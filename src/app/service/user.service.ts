import { Injectable } from '@angular/core';
import { IUser } from '../interfaces/user.interface';
import { BehaviorSubject, Observable } from 'rxjs';

const userInit: IUser = {
  userid: 0,
  username: '',
  name: '',
  email: '',
  rolid: 0
}
@Injectable({
  providedIn: 'root'
})

export class UserService {

  private user$ = new BehaviorSubject<IUser>(userInit);

  constructor() { }

  get userLoged$() : Observable<IUser>{
    return this.user$;
  }

  setUserLogged(user: IUser): void {
    this.user$.next(user);
  }


}
