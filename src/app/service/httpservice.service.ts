import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductoDetail } from '../lib/ejemplo/interface/ProductDetail.interface';
import { enviroment } from '../../enviroments/enviroment';

@Injectable({
  providedIn: 'root'
})
export class HttpProductService {

  private BASE_URL: string = enviroment.API;

  constructor( private http: HttpClient) {}
  readProducts(): Observable<ProductoDetail[]>{
    return this.http.get<ProductoDetail[]>(this.BASE_URL+"/getproducts");
  }

  maintenanceProduct(product: ProductoDetail): Observable<ProductoDetail>{
    return this.http.post<ProductoDetail>(this.BASE_URL+"/maintenanceproduct", product);
  }
}
