import { Injectable } from '@angular/core';
import { ProductoDetail } from '../lib/ejemplo/interface/ProductDetail.interface';
import { BehaviorSubject, Observable } from 'rxjs';

const productoSeleccionado: ProductoDetail = {
  productoid: 0,
  producto: '',
  modelo: '',
  categoria: '',
  precio: 0.00,
  proveedor: '',
  stock: 0
};


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private producto$ = new BehaviorSubject<ProductoDetail>(productoSeleccionado);

  constructor() { }

  get productoSelect(): Observable<ProductoDetail>{
    return this.producto$;
  }

  setProductoSeleccionado(producto: ProductoDetail) : void{
    this.producto$.next(producto);
  }
}
