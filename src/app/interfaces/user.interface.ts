export interface IUser{
    userid: number,
    username: string,
    name: string,
    email: string,
    rolid: number
  }
  