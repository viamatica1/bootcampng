
import { Observable, of } from 'rxjs';
import { EditarproductoComponent } from '../lib/ejemplo/editarproducto/editarproducto.component';
import { CanDeactivateFn } from '@angular/router';

/*export const deactivateGuard: CanActivateFn = (route, state) => {
  return true;
};*/

export const canDeactivateGuard: CanDeactivateFn<EditarproductoComponent> = (componente: EditarproductoComponent): Observable<boolean> => {
  
  if(componente.hayCambios){
    return of(window.confirm(
      '¿Estás seguro de abandonar esta página? Se perderán los cambios no guardados.'
    ));
  }
  
  return of(window.confirm(
      '¿Estás seguro de abandonar esta página? '
    ));
};

