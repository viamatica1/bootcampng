import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TableComponent } from './lib/ejemplo/table/table.component';
import { NotfoundComponent } from './lib/shared/notfound/notfound.component';
import { ListaproductoComponent } from './lib/ejemplo/listaproducto/listaproducto.component';
import { EditarproductoComponent } from './lib/ejemplo/editarproducto/editarproducto.component';
import { authguardGuard } from './guards/authguard.guard';
import { guardcanmatchGuard } from './guards/guardcanmatch.guard';
import { canDeactivateGuard } from './guards/deactivate.guard';


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', 
    loadChildren: () => import('./lib/autenticacion/autenticacion.module').then(m => m.AutenticacionModule)
  },
  {path: 'table', component: TableComponent},
  {path: 'listaproducto', component: ListaproductoComponent,
  canMatch: [guardcanmatchGuard],
  },
  {path: 'editarproducto', component: EditarproductoComponent,
    canActivate: [authguardGuard]
  },
  {path: '**', component: NotfoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
