import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { PropertiesItem } from '../../../interfaces/menuitem.interface';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-menuoffcanvas',
  templateUrl: './menuoffcanvas.component.html'
})
export class MenuoffcanvasComponent implements OnInit{

  constructor(
    private srvUser: UserService, 
    private chgDetector: ChangeDetectorRef
    ){};
  
  idrol!: number; 
  show: boolean = false;

  @Input()
  menuItems!: PropertiesItem[];

  menuItemsRol: PropertiesItem[] = [];

  open(){
    this.show = true;
  }

  close(){
    this.show = false;
  }

  ngOnInit(): void {
    this.srvUser.userLoged$.subscribe( user => {
      for(let i = 0; i < this.menuItems.length; i++){
        if(this.menuItems[i].idrol === user.rolid){
          this.menuItemsRol.push(this.menuItems[i])
        }
      }
    } );
    this.chgDetector.detectChanges();
  }
}
