import { AfterContentChecked, AfterViewChecked, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ProductoDetail } from '../interface/ProductDetail.interface';
import { PropertiesBtnTabla } from '../../../interfaces/botonestabla.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpProductService } from '../../../service/httpservice.service';
import { ProductService } from '../../../service/product.service';

@Component({
  selector: 'app-listaproducto',
  templateUrl: './listaproducto.component.html'
})
export class ListaproductoComponent implements OnInit{

  constructor(private router: Router, 
    private servProducts: HttpProductService,
    private srvProductsLocal: ProductService,
    private cDetector: ChangeDetectorRef,
    private aRoute: ActivatedRoute){}
  

  btnTablaLProductos: PropertiesBtnTabla = {
    bootstrapEditar: "success",
    bootstrapEliminar: "danger",
    labelEditar: "Editar producto",
    labelEliminar: "Eliminar producto",
    mostrarEditar: true,
    mostrarEliminar: true
  };

  listaproducto: ProductoDetail[] = [];


  editarProducto(producto: ProductoDetail){
    //this.router.navigate(['/editarproducto'], {queryParams: {idproducto: producto.productoid}});
    this.srvProductsLocal.setProductoSeleccionado(producto);
    this.router.navigate(['editarproducto']);
  }

  eliminarProducto(producto: ProductoDetail){

  }

  ngOnInit(): void {
    this.consultarProductos();
  }

  consultarProductos(){
    this.listaproducto = [];
    this.servProducts.readProducts().subscribe( lproductos => {
      this.listaproducto = lproductos;
    });
    this.cDetector.detectChanges();
  }

  nuevoProducto(producto: ProductoDetail): void{
    producto.productoid = this.listaproducto.length + 1;
    this.servProducts.maintenanceProduct(producto).subscribe ( productoagregado => {
      console.log(productoagregado);
    });
    this.cDetector.detectChanges();
    
    

  }

  mensajeCerrar(event: any){

  }

  estaVacio(value: any): boolean{
    return value === undefined || value === null || value === '';
  }

}
