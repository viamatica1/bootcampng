import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ProductoDetail } from '../interface/ProductDetail.interface';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductService } from '../../../service/product.service';
import { Alert } from 'bootstrap';

@Component({
  selector: 'app-editarproducto',
  templateUrl: './editarproducto.component.html',
})
export class EditarproductoComponent implements OnInit{

  idproducto!: number;
  nombreProducto!: string;

  hayCambios: boolean = false;


  constructor(/*private readonly router: ActivatedRoute*/ private srvProduct: ProductService,
  private chgDetector: ChangeDetectorRef){}

/*
  listaproducto: ProductoDetail[] = [
  {
    "productoid": 1,
    "producto": "Play Station 5",
    "modelo": "Ultra Slim",
    "proveedor": "H&B sa",
    "precio": 600.00,
    "stock": 40,
    "categoria": "OC"
  },
  {
    "productoid": 2,
    "producto": "PC Gammer",
    "modelo": "AS-001-wm",
    "proveedor": "Asus",
    "precio": 850.00,
    "stock": 2000,
    "categoria": "OC"
  },
  {
    "productoid": 3,
    "producto": "MousePad",
    "modelo": "MP-2001-A",
    "proveedor": "Juan Marcet",
    "precio": 10.00,
    "stock": 3,
    "categoria": "VA"
  },
  {
    "productoid": 4,
    "producto": "Doppler",
    "modelo": "2HXDB",
    "proveedor": "General Electric",
    "precio": 120.00,
    "stock": 11,
    "categoria": "HT"
  },
  {
    "productoid": 5,
    "producto": "AirFried",
    "modelo": "ZY 2020",
    "proveedor": "Hometech",
    "precio": 230.00,
    "stock": 20,
    "categoria": "CO"
  },
  {
    "productoid": 6,
    "producto": "Parlante",
    "modelo": "ABC 123",
    "proveedor": "Bosse",
    "precio": 190.00,
    "stock": 1,
    "categoria": "VA"
  },
  {
    "productoid": 7,
    "producto": "Aro de luz",
    "modelo": "XYZ 987",
    "proveedor": "Castell",
    "precio": 90.00,
    "stock": 11,
    "categoria": "VA"
  }
];*/

productoSeleccionado!: ProductoDetail;
  /*productoSeleccionado: ProductoDetail = {
  productoid: 0,
  producto: '',
  modelo: '',
  categoria: '',
  precio: 0.00,
  proveedor: '',
  stock: 0
};*/

  ngOnInit(): void{
    this.srvProduct.productoSelect.subscribe(prodSeleccionado => this.productoSeleccionado = prodSeleccionado);
    this.chgDetector.detectChanges();
    /*this.router.queryParams.subscribe(
      (params: Params) =>{
        this.idproducto = params['idproducto'];
        this.nombreProducto = params['nombre'];
      }
    );
    this.productoSeleccionado = this.listaproducto.find( producto => producto.productoid == this.idproducto)!;
    */
  }


   detectarCambios(){
    this.hayCambios = true;
    console.log('Hay cambios');
   }


   
}
