export interface ProductoDetail{
    productoid: number,
    producto: string,
    modelo: string,
    proveedor: string,
    precio: number,
    stock: number,
    categoria: string,
}