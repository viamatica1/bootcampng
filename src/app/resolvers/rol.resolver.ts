import {Resolve } from "@angular/router";
import { Observable, of } from "rxjs";

export class RolResolver implements Resolve<any>{

    private readonly rolUser: number = 1;
    resolve(): Observable<any> {
        return of(this.rolUser);
    }

}